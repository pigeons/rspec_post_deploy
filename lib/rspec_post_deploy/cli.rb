require 'optparse'
require 'rspec_post_deploy/init/initialize'
require 'rspec_post_deploy/init/environment'

module RspecPostDeploy
  class CLI
    class << self
      def invoke(args)
        optparse = OptionParser.new do |opts|
          opts.banner = 'Usage: rspec_post_deploy [options]'

          opts.on('-i', '--init', 'initialize a new project') do
            
          end

          opts.on('-a', '--add-environment=name', 'add a new test environment') do |name|
            
          end

          opts.on('-h', '--help', 'Show help message') do
            puts opts
            exit 0
          end
        end

        args.shift
        options = optparse.parse(args)

        RoboPigeon::Extensions::Template.render(name, path, template)
      end
    end
  end
end
