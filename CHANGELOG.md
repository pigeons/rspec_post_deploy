## 0.1.0 (2019-08-15)

### Added

- Initial Gem Release
- Uses Encryptatron for configuration
- Uses Capybara for web testing
- Has Rails generator
- Has a bin script that generates a basic project
